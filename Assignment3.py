import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import animation

fig = plt.figure(1)
ax = fig.add_subplot(1, 1, 1, projection='3d')  # 指定三维空间做图

t = np.linspace(0, 40, 2000)  # 在0到4之间，均匀产生200点的数组

theta = t * 2 * np.pi  # 角度
Z=[]
Z.append(0)
# 生成曲线数组
z = t
x = np.sin(theta)
y = np.cos(theta)

# 运动的点
point, = ax.plot([x[0]], [y[0]], [z[0]], 'ro', label='p')

# 曲线
line, = ax.plot([x[0]], [y[0]], [z[0]], label='line')

# 设置显示的范围和描述
x_min = 0
y_min = 0
z_min = 0
x_max = 1
y_max = 1
z_max = 5
margin = 1
ax.set_xlim(x_min - margin, x_max + margin)
ax.set_ylim(y_min - margin, y_max + margin)
ax.set_zlim(z_min - margin, z_max + margin)
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
# 标题
ax.set_title('3D animate')
ax.view_init(30, 35)
# 设置标签在右下角
ax.legend(loc='lower right')


def animate(i):
    point.set_xdata(x[i])
    point.set_ydata(y[i])
    point.set_3d_properties(0)
    line.set_xdata(x[:i+1 ])
    line.set_ydata(y[:i+1 ])
    for j in range(0,i+1):
        Z[j]+=1/40
    Z.append(0)
    line.set_3d_properties(Z[:i + 1])

ani = animation.FuncAnimation(fig=fig,
                              func=animate,
                              frames=len(x),
                              interval=50,
                              repeat=False,
                              blit=False)

plt.show()