import math

import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
from math import *
fig = plt.figure()
ax = plt.axes(xlim=(0, 50), ylim=(-2, 2))

line, = ax.plot([], [], lw=2,color='blue')
point, = ax.plot([], [], 'ro', label='p')

X=np.linspace(0, 0, 2000)
Y=np.linspace(0, 2000, 2000)

Zeta=0.05
Omega=0.05
A=1.5

ANIMATION=True

def init():
    line.set_data([], [])
    return line,

def animate(i):
    #y = A * np.cos(0.05 * i)
    y = A * exp(-Zeta * Omega * i) * np.cos(math.sqrt(1-Zeta*Zeta) * Omega * i)
    point.set_xdata(0)
    point.set_ydata(y)
    Y[i] = y

    line.set_xdata(X[:i])
    line.set_ydata(Y[:i])
    for i in range(0,i+1):
        X[i]+=1/40*np.pi
    label = "timestep{0}".format(i)
    ax.set_title(label)
    return point,line,ax,

#将fig挂在动画上面
if(ANIMATION):
    anim = animation.FuncAnimation(fig, animate, init_func=init,frames=20000, interval=100, blit=False)
else:
    frame=1500
    for i in range(0,frame+1):
        y = A * exp(-Zeta * Omega * i) * np.cos(math.sqrt(1 - Zeta * Zeta) * Omega * i)
        X[i] += i / 40 * np.pi
        Y[i] = y
    plt.plot(X[:frame],Y[:frame])
    #plt.scatter(0,A * exp(-Zeta * Omega * frame) * np.cos(math.sqrt(1 - Zeta * Zeta) * Omega * frame))
#如果需要保存动画，就这样
#anim.save('basic_animation.mp4', fps=30, extra_args=['-vcodec', 'libx264'])
#标题名称
plt.xlabel('x')
plt.ylabel('y')
plt.show()
